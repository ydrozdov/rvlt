package com.ydrozdov.revolut;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.ydrozdov.revolut.repository.AccountInMemoryRepository;
import com.ydrozdov.revolut.repository.AccountRepository;
import com.ydrozdov.revolut.repository.TransferInMemoryRepository;
import com.ydrozdov.revolut.repository.TransferRepository;
import com.ydrozdov.revolut.routing.AccountRouting;
import com.ydrozdov.revolut.routing.Routing;
import com.ydrozdov.revolut.routing.TransferRouting;
import com.ydrozdov.revolut.service.TransferService;
import io.javalin.Javalin;
import io.javalin.core.util.RouteOverviewPlugin;

/**
 * This class is a Guice module that tells Guice how to bind several
 * different types. This Guice module is created when the
 * application starts.
 */
public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Javalin.class).toInstance(
            Javalin.create(config -> config.registerPlugin(new RouteOverviewPlugin("/path")))
        );
        bind(AppEntryPoint.class);
        bind(TransferService.class);
        bind(AccountRepository.class).to(AccountInMemoryRepository.class);
        bind(TransferRepository.class).to(TransferInMemoryRepository.class);
        Multibinder.newSetBinder(binder(), Routing.class).addBinding().to(TransferRouting.class);
        Multibinder.newSetBinder(binder(), Routing.class).addBinding().to(AccountRouting.class);
    }
}
