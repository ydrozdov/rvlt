package com.ydrozdov.revolut;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Main class of an application. Creates Guice injector and launches an application
 */
public class App {
    public static final int APP_PORT = 7000;

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new AppModule());
        injector.getInstance(AppEntryPoint.class).start(APP_PORT);
    }
}
