package com.ydrozdov.revolut;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.exception.InsufficientFundsException;
import com.ydrozdov.revolut.exception.InvalidBeneficiaryException;
import com.ydrozdov.revolut.exception.InvalidSenderException;
import com.ydrozdov.revolut.exception.InvalidTransferIdException;
import com.ydrozdov.revolut.routing.Routing;
import io.javalin.Javalin;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import org.modelmapper.MappingException;

/**
 * Class that can be used to bootstrap and launch an application
 */
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
@Slf4j
public class AppEntryPoint {

    private Javalin app;
    private Set<Routing> routes;

    public void start(int port) {
        bindRoutes();
        app.start(port);
        handleExceptions();
    }

    public void stop() {
        app.stop();
    }

    private void bindRoutes() {
        routes.forEach(Routing::bindRoutes);
    }

    private void handleExceptions() {
        app.exception(InsufficientFundsException.class, (e, ctx) ->
            ctx.status(HttpStatus.BAD_REQUEST_400)
        );

        app.exception(InvalidBeneficiaryException.class, (e, ctx) ->
            ctx.status(HttpStatus.BAD_REQUEST_400)
        );

        app.exception(InvalidSenderException.class, (e, ctx) ->
            ctx.status(HttpStatus.BAD_REQUEST_400)
        );

        app.exception(InvalidTransferIdException.class, (e, ctx) ->
            ctx.status(HttpStatus.BAD_REQUEST_400)
        );

        app.exception(MappingException.class, (e, ctx) ->
            ctx.status(HttpStatus.BAD_REQUEST_400)
        );
    }
}
