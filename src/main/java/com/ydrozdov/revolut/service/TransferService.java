package com.ydrozdov.revolut.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.exception.InsufficientFundsException;
import com.ydrozdov.revolut.exception.InvalidBeneficiaryException;
import com.ydrozdov.revolut.exception.InvalidSenderException;
import com.ydrozdov.revolut.exception.InvalidTransferIdException;
import com.ydrozdov.revolut.model.Account;
import com.ydrozdov.revolut.model.Transfer;
import com.ydrozdov.revolut.repository.AccountRepository;
import com.ydrozdov.revolut.repository.TransferRepository;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
public class TransferService {
    private TransferRepository transferRepository;
    private AccountRepository accountRepository;

    public Transfer execute(Transfer transfer) {
        UUID senderId = transfer.getSenderId();
        log.debug("Sender id: {}", senderId);
        Account sender = accountRepository.findById(senderId)
            .orElseThrow(InvalidSenderException::new);

        UUID beneficiaryId = transfer.getBeneficiaryId();
        log.debug("Beneficiary id: {}", beneficiaryId);
        Account beneficiary = accountRepository.findById(beneficiaryId)
            .orElseThrow(InvalidBeneficiaryException::new);

        synchronized (sender) {
            if (sender.getBalance().subtract(transfer.getAmount()).compareTo(BigDecimal.ZERO) < 0) {
                throw new InsufficientFundsException();
            }

            sender.withdraw(transfer.getAmount());
            beneficiary.deposit(transfer.getAmount());
            accountRepository.save(sender);
            accountRepository.save(beneficiary);
        }

        return transferRepository.save(transfer);
    }

    public Transfer get(UUID transferId) {
        return transferRepository.findById(transferId).orElseThrow(InvalidTransferIdException::new);
    }
}
