package com.ydrozdov.revolut.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.model.Account;
import com.ydrozdov.revolut.repository.AccountRepository;
import lombok.AllArgsConstructor;

@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
public class AccountService {
    private AccountRepository accountRepository;

    public Account save(Account account) {
        return accountRepository.save(account);
    }
}
