package com.ydrozdov.revolut.routing;

import com.google.inject.Inject;
import com.google.inject.Injector;

public abstract class Routing<T> {
    @Inject
    private Injector injector;

    public T getController() {
        return injector.getInstance(getControllerClass());
    }

    public abstract void bindRoutes();

    protected abstract Class<T> getControllerClass();

    protected Routing() {
    }
}
