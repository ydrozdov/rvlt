package com.ydrozdov.revolut.routing;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.controller.TransferController;
import io.javalin.Javalin;
import lombok.AllArgsConstructor;

import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.post;

/**
 * Groups transfer endpoint handlers
 */
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
public class TransferRouting extends Routing<TransferController> {
    private Javalin javalin;

    @Override
    public void bindRoutes() {
        javalin.routes(() ->
            path("transfers", () -> {
                post(ctx -> getController().create(ctx));
                get("/:transferId", ctx -> getController().get(ctx));
            })
        );
    }

    @Override
    protected Class<TransferController> getControllerClass() {
        return TransferController.class;
    }
}
