package com.ydrozdov.revolut.routing;

import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.controller.AccountController;
import io.javalin.Javalin;
import lombok.AllArgsConstructor;

/**
 * Groups account endpoints handlers
 */
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
public class AccountRouting extends Routing<AccountController>{
    private Javalin javalin;

    @Override
    public void bindRoutes() {
        javalin.routes(() ->
            path("accounts", () -> post(ctx -> getController().create(ctx)))
        );
    }

    @Override
    protected Class<AccountController> getControllerClass() {
        return AccountController.class;
    }
}
