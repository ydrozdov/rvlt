package com.ydrozdov.revolut.repository;

import com.google.inject.Singleton;
import com.ydrozdov.revolut.model.Transfer;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.NonNull;

/**
 * In memory implementation of TransferRepository
 */
@Singleton
public class TransferInMemoryRepository implements TransferRepository {
    private ConcurrentMap<UUID, Transfer> transfers = new ConcurrentHashMap<>();

    @Override
    public Transfer save(@NonNull Transfer entity) {
        Transfer entityCopy = new Transfer(entity);
        transfers.put(entityCopy.getId(), entityCopy);
        return entity;
    }

    @Override
    public Optional<Transfer> findById(@NonNull UUID uuid) {
        Transfer transfer = transfers.get(uuid);
        if (transfer == null) {
            return Optional.empty();
        }

        return Optional.of(new Transfer(transfer));
    }

    @Override
    public int count() {
        return transfers.size();
    }
}
