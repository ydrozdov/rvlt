package com.ydrozdov.revolut.repository;

import com.ydrozdov.revolut.model.Account;
import java.util.UUID;

/**
 * Interface captures Account type. General purpose is to hold type information.
 */
public interface AccountRepository extends Repository<Account, UUID> {

}
