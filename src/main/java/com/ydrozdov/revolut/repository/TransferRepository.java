package com.ydrozdov.revolut.repository;

import com.ydrozdov.revolut.model.Transfer;
import java.util.UUID;

/**
 * Interface captures Transfer type. General purpose is to hold type information.
 */
public interface TransferRepository extends Repository<Transfer, UUID> {

}
