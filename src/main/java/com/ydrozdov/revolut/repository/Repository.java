package com.ydrozdov.revolut.repository;

import java.util.Optional;

/**
 * Interface for generic create and read operations
 *
 * @param <T>
 * @param <I>
 */
public interface Repository<T, I> {
    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation
     * might have changed the entity instance completely.
     *
     * @param entity must not be {@literal null}.
     * @return the saved entity; will never be {@literal null}.
     * @throws IllegalArgumentException in case the given {@literal entity} is {@literal null}.
     */
    T save(T entity);

    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be {@literal null}.
     * @return the entity with the given id or {@literal Optional#empty()} if none found.
     * @throws IllegalArgumentException if {@literal id} is {@literal null}.
     */
    Optional<T> findById(I id);

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities.
     */
    int count();
}
