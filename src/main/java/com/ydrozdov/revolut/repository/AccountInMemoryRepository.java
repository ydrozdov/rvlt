package com.ydrozdov.revolut.repository;

import com.google.inject.Singleton;
import com.ydrozdov.revolut.model.Account;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import lombok.NonNull;

/**
 * In memory implementation of AccountRepository
 */
@Singleton
public class AccountInMemoryRepository implements AccountRepository {
    private ConcurrentMap<UUID, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public Account save(@NonNull Account entity) {
        Account entityCopy = new Account(entity);
        accounts.put(entityCopy.getId(), entityCopy);
        return entity;
    }

    @Override
    public Optional<Account> findById(@NonNull UUID uuid) {
        Account account = accounts.get(uuid);
        if (account == null) {
            return Optional.empty();
        }

        return Optional.of(new Account(account));
    }

    @Override
    public int count() {
        return accounts.size();
    }
}
