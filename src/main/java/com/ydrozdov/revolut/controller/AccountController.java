package com.ydrozdov.revolut.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.controller.dto.AccountDTO;
import com.ydrozdov.revolut.model.Account;
import com.ydrozdov.revolut.service.AccountService;
import io.javalin.http.Context;
import lombok.AllArgsConstructor;
import org.eclipse.jetty.http.HttpStatus;
import org.modelmapper.ModelMapper;

/**
 * Controller to handle account related actions
 */
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
public class AccountController {
    private AccountService accountService;
    private ModelMapper modelMapper;

    /**
     * Creates an account
     * @param ctx
     */
    public void create(Context ctx) {
        AccountDTO accountDTO = ctx.bodyValidator(AccountDTO.class)
            .check(obj -> obj.getBalance() >= 0, "Balance should be positive or zero.")
            .get();

        Account account = accountService.save(mapToEntity(accountDTO));

        ctx.status(HttpStatus.CREATED_201);
        ctx.json(mapToDTO(account));
    }

    private Account mapToEntity(AccountDTO accountDTO) {
        return modelMapper.map(accountDTO, Account.class);
    }

    private AccountDTO mapToDTO(Account account) {
        return modelMapper.map(account, AccountDTO.AccountDTOBuilder.class).build();
    }
}
