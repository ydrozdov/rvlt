package com.ydrozdov.revolut.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.ydrozdov.revolut.controller.dto.TransferDTO;
import com.ydrozdov.revolut.model.Transfer;
import com.ydrozdov.revolut.service.TransferService;
import io.javalin.http.Context;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.http.HttpStatus;
import org.modelmapper.ModelMapper;

/**
 * Controller to handle transfer related actions
 */
@Singleton
@AllArgsConstructor(onConstructor = @__({ @Inject}))
@Slf4j
public class TransferController {
    public static final String REGEX_UUID = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

    private TransferService transferService;
    private ModelMapper modelMapper;

    /**
     * Validates input and creates transfer
     * @param ctx
     */
    public void create(Context ctx) {
        TransferDTO transferDTO = ctx.bodyValidator(TransferDTO.class)
            .check(t -> t.getAmount() > 0, "Transfer amount should be positive")
            .check(t -> t.getSenderId().matches(REGEX_UUID), "Invalid senderId")
            .check(t -> t.getBeneficiaryId().matches(REGEX_UUID), "Invalid beneficiaryId")
            .get();

        log.debug("transferDTO: {}", transferDTO);
        Transfer transfer = mapToEntity(transferDTO);
        log.debug("transfer: {}", transfer);

        Transfer result = transferService.execute(transfer);

        ctx.status(HttpStatus.CREATED_201);
        ctx.json(mapToDTO(result));
    }

    /**
     * Gives a transfer by transfer id
     * @param ctx
     */
    public void get(Context ctx) {
        UUID transferId = UUID.fromString(ctx.pathParam("transferId", String.class)
            .check(t -> t.matches(REGEX_UUID), "Invalid transferId")
            .get()
        );
        log.debug("Transfer id: {}", transferId);
        Transfer transfer = transferService.get(transferId);
        log.debug("transfer: {}", transfer);
        ctx.json(mapToDTO(transfer));
    }

    private Transfer mapToEntity(TransferDTO transferDTO) {
        return modelMapper.map(transferDTO, Transfer.TransferBuilder.class)
            .build();
    }

    private TransferDTO mapToDTO(Transfer transfer) {
        return modelMapper.map(transfer, TransferDTO.TransferDTOBuilder.class).build();
    }
}
