package com.ydrozdov.revolut.controller.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(setterPrefix = "set")
public class AccountDTO {
    private final String id;
    private final double balance;

    @JsonCreator
    public AccountDTO(
        @JsonProperty("id") String id,
        @JsonProperty("balance") double balance
    ) {
        this.id = id;
        this.balance = balance;
    }
}
