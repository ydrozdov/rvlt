package com.ydrozdov.revolut.controller.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@Builder(setterPrefix = "set")
public class TransferDTO {
    private final String id;
    private final String senderId;
    private final String beneficiaryId;
    private final double amount;

    @JsonCreator
    public TransferDTO(
        @JsonProperty("senderId") String senderId,
        @JsonProperty("beneficiaryId") String beneficiaryId,
        @JsonProperty("amount") double amount
    ) {
        this(null, senderId, beneficiaryId, amount);
    }
}
