package com.ydrozdov.revolut.model;

import java.math.BigDecimal;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

@ToString
@EqualsAndHashCode
@Getter
@AllArgsConstructor
@Slf4j
public class Transfer {
    private final UUID id;
    private final UUID senderId;
    private final UUID beneficiaryId;
    private final BigDecimal amount;

    public Transfer(@NotNull Transfer transfer) {
        this(
            transfer.getId(),
            transfer.getSenderId(),
            transfer.getBeneficiaryId(),
            transfer.getAmount()
        );
    }

    public static TransferBuilder builder() {
        return new TransferBuilder();
    }

    @NoArgsConstructor
    @ToString
    @Setter
    @Accessors(fluent = true, chain = true)
    public static class TransferBuilder {

        private UUID id;
        private UUID senderId;
        private UUID beneficiaryId;
        private BigDecimal amount;

        public TransferBuilder setSenderId(String senderId) {
            return senderId(UUID.fromString(senderId));
        }

        public TransferBuilder setBeneficiaryId(String beneficiaryId) {
            return beneficiaryId(UUID.fromString(beneficiaryId));
        }

        public TransferBuilder setAmount(double amount) {
            return amount(BigDecimal.valueOf(amount));
        }

        public Transfer build() {
            if (id == null) {
                id = UUID.randomUUID();
            }

            return new Transfer(id, senderId, beneficiaryId, amount);
        }
    }
}
