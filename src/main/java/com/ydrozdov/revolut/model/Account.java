package com.ydrozdov.revolut.model;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Data;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;

@Data
public class Account {
    private UUID id;
    private AtomicReference<BigDecimal> balance;

    public Account() {
        this.id = UUID.randomUUID();
        this.balance = new AtomicReference<>();
        this.balance.set(BigDecimal.ZERO);
    }

    public Account(@NotNull Account account) {
        this.id = account.getId();
        this.balance = new AtomicReference<>();
        this.balance.set(account.getBalance());
    }

    public BigDecimal getBalance() {
        return this.balance.get();
    }

    public void setBalance(double balance) {
        this.setBalance(BigDecimal.valueOf(balance));
    }

    public void setBalance(@NonNull BigDecimal balance) {
        this.balance.set(balance);
    }

    public void withdraw(@NonNull BigDecimal amount) {
        this.balance.accumulateAndGet(amount.abs(), BigDecimal::subtract);
    }

    public void deposit(@NonNull BigDecimal amount) {
        this.balance.accumulateAndGet(amount.abs(), BigDecimal::add);
    }
}
