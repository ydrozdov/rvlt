package com.ydrozdov.revolut.exception;

/**
 * Thrown to indicate that sender is inappropriate for an operation
 */
public class InvalidSenderException extends RuntimeException {

}
