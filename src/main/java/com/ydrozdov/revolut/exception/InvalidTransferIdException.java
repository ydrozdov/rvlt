package com.ydrozdov.revolut.exception;

/**
 * Thrown to indicate that transfer id is inappropriate for an operation
 */
public class InvalidTransferIdException extends RuntimeException {

}
