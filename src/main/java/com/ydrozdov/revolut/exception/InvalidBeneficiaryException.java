package com.ydrozdov.revolut.exception;

/**
 * Thrown to indicate that beneficiary is inappropriate for an operation
 */
public class InvalidBeneficiaryException extends RuntimeException {

}
