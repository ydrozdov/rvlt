package com.ydrozdov.revolut.exception;

/**
 * Thrown to indicate that balance is inappropriate for an operation
 */
public class InsufficientFundsException extends RuntimeException {

}
