package com.ydrozdov.revolut.service

import com.ydrozdov.revolut.exception.InsufficientFundsException
import com.ydrozdov.revolut.exception.InvalidBeneficiaryException
import com.ydrozdov.revolut.exception.InvalidSenderException
import com.ydrozdov.revolut.exception.InvalidTransferIdException
import com.ydrozdov.revolut.model.Account
import com.ydrozdov.revolut.model.Transfer
import com.ydrozdov.revolut.repository.AccountRepository
import com.ydrozdov.revolut.repository.TransferRepository
import org.jmock.lib.concurrent.Blitzer
import spock.lang.Specification
import spock.lang.Unroll

class TransferServiceTest extends Specification {
    static final int ACTION_COUNT = 25000
    Blitzer blitzer = new Blitzer(ACTION_COUNT)

    def "Get should not throw an exception when transfer is returned by transfer repository"() {
        given:
        def transferId = UUID.randomUUID()
        def transfer =  createTransfer(transferId)
        def transferRepository = Mock(TransferRepository) {
            1 * findById(transferId) >> Optional<Transfer>.of(transfer)
        }
        def accountService = Mock(AccountRepository)
        def transferService = new TransferService(transferRepository, accountService)

        when: "get is called"
        transferService.get(transferId)

        then: "no InvalidTransferIdException is thrown"
        notThrown(InvalidTransferIdException)
    }

    def "Get should throw an exception when there is no transfer returned by transfer repository"() {
        given:
        def nonExistingTransferId = UUID.randomUUID()
        def transferRepository = Mock(TransferRepository) {
            1 * findById(nonExistingTransferId) >> Optional<Transfer>.empty()
        }
        def accountService = Mock(AccountRepository)
        def transferService = new TransferService(transferRepository, accountService)

        when: "get is called with non-existing transfer"
        transferService.get(nonExistingTransferId)

        then: "InvalidTransferIdException is thrown"
        thrown(InvalidTransferIdException)
    }

    def "Execute should throw an exception when sender is not found"() {
        given:
        def transfer = createTransfer()
        def transferRepository = Mock(TransferRepository)
        def accountService = Mock(AccountRepository) {
            1 * findById(transfer.senderId) >> Optional<Account>.empty()
        }
        def transferService = new TransferService(transferRepository, accountService)

        when: "execute with invalid sender is called"
        transferService.execute(transfer)

        then: "InvalidSenderException is thrown"
        thrown(InvalidSenderException)
    }

    def "Execute should throw an exception when beneficiary is not found"() {
        given:
        def transfer = createTransfer()
        def transferRepository = Mock(TransferRepository)
        def sender = new Account()
        def accountService = Mock(AccountRepository) {
            1 * findById(transfer.senderId) >> Optional<Account>.of(sender)
            1 * findById(transfer.beneficiaryId) >> Optional<Account>.empty()
        }
        def transferService = new TransferService(transferRepository, accountService)

        when: "execute with invalid beneficiary is called"
        transferService.execute(transfer)

        then: "InvalidBeneficiaryException is thrown"
        thrown(InvalidBeneficiaryException)
    }

    def "Execute throws an exception when sender's balance is too low"(senderBalance, transferAmount) {
        given:
        def transfer = createTransfer(UUID.randomUUID(), BigDecimal.valueOf(transferAmount))
        def transferRepository = Mock(TransferRepository)
        def sender = new Account()
        sender.setBalance(senderBalance)
        def beneficiary = new Account()
        def accountService = Mock(AccountRepository) {
            1 * findById(transfer.senderId) >> Optional<Account>.of(sender)
            1 * findById(transfer.beneficiaryId) >> Optional<Account>.of(beneficiary)
        }
        def transferService = new TransferService(transferRepository, accountService)

        when: "execute with sender's insufficient balance is called"
        transferService.execute(transfer)

        then: "InsufficientFundsException is thrown"
        thrown(InsufficientFundsException)

        where:
        senderBalance | transferAmount
        0             | 1
        10            | 20
        10.0          | 10.1
    }

    @Unroll
    def "Execute calls withdraw and deposit of sender and beneficiary"(senderBalance, transferAmount) {
        given:
        def transfer = createTransfer(UUID.randomUUID(), BigDecimal.valueOf(transferAmount))
        def transferRepository = Mock(TransferRepository)
        def sender = Mock(Account) {
            1 * getBalance() >> senderBalance
            1 * withdraw(BigDecimal.valueOf(transferAmount))
        }
        def beneficiary =  Mock(Account) {
            1 * deposit(BigDecimal.valueOf(transferAmount))
        }
        def accountService = Mock(AccountRepository) {
            1 * findById(transfer.senderId) >> Optional<Account>.of(sender)
            1 * findById(transfer.beneficiaryId) >> Optional<Account>.of(beneficiary)
        }
        def transferService = new TransferService(transferRepository, accountService)

        when: "execute is called"
        transferService.execute(transfer)

        then:
        1 * transferRepository.save(transfer)

        where:
        senderBalance | transferAmount
        2             | 1
        30            | 20
        11.0          | 10.1
    }

    def "Execute with multiple threads should produce correct results"() {
        given:
        def transfer = createTransfer(UUID.randomUUID(), BigDecimal.ONE)
        def sender = new Account()
        sender.setBalance(ACTION_COUNT)
        def beneficiary = new Account()
        def transferRepository = Mock(TransferRepository)
        def accountService = Mock(AccountRepository) {
            _ * findById(transfer.senderId) >> Optional<Account>.of(sender)
            _ * findById(transfer.beneficiaryId) >> Optional<Account>.of(beneficiary)
        }
        def transferService = new TransferService(transferRepository, accountService)

        when:
        blitzer.blitz( () -> {
            transferService.execute(transfer)
        })

        then:
        sender.getBalance() == BigDecimal.ZERO
        beneficiary.getBalance() == BigDecimal.valueOf(ACTION_COUNT)

    }

    static def createTransfer(
            transferId = UUID.randomUUID(),
            transferAmount = BigDecimal.ONE
    ) {
        return Transfer.builder()
            .id(transferId)
            .amount(transferAmount)
            .beneficiaryId(UUID.randomUUID())
            .senderId(UUID.randomUUID())
            .build()
    }
}
