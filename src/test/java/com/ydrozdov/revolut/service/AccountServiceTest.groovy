package com.ydrozdov.revolut.service

import com.ydrozdov.revolut.model.Account
import com.ydrozdov.revolut.repository.AccountRepository
import spock.lang.Specification

class AccountServiceTest extends Specification {
    def "Save should call repository"() {
        given:
        def accountRepository = Mock(AccountRepository)
        def account = Mock(Account)
        def accountService = new AccountService(accountRepository)

        when: "Save is called"
        accountService.save(account)

        then: "repository's save is called"
        1 * accountRepository.save(account)
    }
}
