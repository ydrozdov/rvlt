package com.ydrozdov.revolut.repository

import com.ydrozdov.revolut.model.Account
import com.ydrozdov.revolut.model.Transfer
import org.jmock.lib.concurrent.Blitzer
import spock.lang.Specification

class TransferInMemoryRepositoryTest extends Specification {
    static final int ACTION_COUNT = 25000
    Blitzer blitzer = new Blitzer(ACTION_COUNT)

    def "Save with null should throw an exception"() {
        given:
        def transferInMemoryRepository = new TransferInMemoryRepository()

        when: "save with null is called"
        transferInMemoryRepository.save(null)

        then: "an exception is thrown"
        thrown(NullPointerException)
    }

    def "Save should add an entry"() {
        given:
        def transfer = createTransfer()

        when: "repository is created"
        def transferInMemoryRepository = new TransferInMemoryRepository()

        then: "initial count is zero"
        transferInMemoryRepository.count() == 0

        when: "an entry is added"
        transferInMemoryRepository.save(transfer)
        Transfer entity = transferInMemoryRepository.findById(transfer.getId()).orElse(null)

        then: "entry is added"
        transferInMemoryRepository.count() == 1
        entity != null
        entity == transfer
    }

    def "FindById should return no value if there is no entry"() {
        when: "new repository created"
        def transferInMemoryRepository = new TransferInMemoryRepository()
        def optional = transferInMemoryRepository.findById(UUID.randomUUID())

        then: "repository has no entries and optional is empty"
        transferInMemoryRepository.count() == 0
        optional.isPresent() == false
    }

    def "FindById with null should throw an exception"() {
        given:
        def transferInMemoryRepository = new TransferInMemoryRepository()

        when: "findById is called with null"
        transferInMemoryRepository.findById(null)

        then: "an exception is thrown"
        thrown(NullPointerException)
    }

    def "FindById returns an entity"() {
        given:
        def transferInMemoryRepository = new TransferInMemoryRepository()
        def transfer = createTransfer()
        transferInMemoryRepository.save(transfer)

        when: "findById is called"
        def optionalEntry = transferInMemoryRepository.findById(transfer.id)

        then: "an entry is returned"
        optionalEntry.isPresent() == true
        optionalEntry.get() == transfer
    }

    def "Save with multiple threads should produce correct results"() {
        given:
        def transferInMemoryRepository = new TransferInMemoryRepository()

        when:
        blitzer.blitz( () -> {
            transferInMemoryRepository.save(createTransfer())
        })

        then:
        blitzer.totalActionCount() == transferInMemoryRepository.count()
    }

    static def createTransfer() {
        return Transfer.builder()
            .id(UUID.randomUUID())
            .senderId(UUID.randomUUID())
            .beneficiaryId(UUID.randomUUID())
            .amount(BigDecimal.ONE)
            .build()
    }
}
