package com.ydrozdov.revolut.repository

import com.ydrozdov.revolut.model.Account
import org.jmock.lib.concurrent.Blitzer
import spock.lang.Specification

class AccountInMemoryRepositoryTest extends Specification {
    static final int ACTION_COUNT = 25000
    Blitzer blitzer = new Blitzer(ACTION_COUNT)

    def "Save should add an entity"() {
        given:
        def account = new Account();

        when: "new repository created"
        def accountInMemoryRepository = new AccountInMemoryRepository()

        then: "there should be no entries"
        accountInMemoryRepository.count() == 0

        when: "We save new entry"
        accountInMemoryRepository.save(account)
        Account entity = accountInMemoryRepository.findById(account.getId()).orElse(null)

        then: "entry is saved and we can find it"
        accountInMemoryRepository.count() == 1
        entity != null
        entity.getId() == account.getId()
        entity.getBalance() == account.getBalance()
    }

    def "Save should throw an exception when trying to save null"() {
        given:
        def accountInMemoryRepository = new AccountInMemoryRepository()

        when: "We try to save null"
        accountInMemoryRepository.save(null)

        then: "exception is thrown"
        thrown(NullPointerException)
    }

    def "FindById should return no value if there is no entry"() {
        when: "new repository created"
        def accountInMemoryRepository = new AccountInMemoryRepository()
        def optional = accountInMemoryRepository.findById(UUID.randomUUID())

        then: "repository has no entries and optional is empty"
        accountInMemoryRepository.count() == 0
        optional.isPresent() == false
    }

    def "FindById returns an entity"() {
        given:
        def account = new Account()
        def anotherAccount = new Account()
        anotherAccount.setBalance(BigDecimal.ONE)
        def accountInMemoryRepository = new AccountInMemoryRepository()
        accountInMemoryRepository.save(account)
        accountInMemoryRepository.save(anotherAccount)

        when: "findById is called"
        def accountOptional = accountInMemoryRepository.findById(account.getId())

        then: "a proper entity is returned"
        accountOptional.isPresent() == true
        accountOptional.get().getId() == account.getId()

        when: "findById is called again"
        def anoutherAccountOptional = accountInMemoryRepository.findById(anotherAccount.getId())

        then: "a proper entity is returned"
        anoutherAccountOptional.isPresent() == true
        anoutherAccountOptional.get().getId() == anotherAccount.getId()
        anoutherAccountOptional.get().getBalance() == anotherAccount.getBalance()
    }

    def "FindById with null should throw an exception"() {
        given:
        def accountInMemoryRepository = new AccountInMemoryRepository()

        when: "findById is called with null"
        accountInMemoryRepository.findById(null)

        then: "an exception is thrown"
        thrown(NullPointerException)
    }

    def "Save with multiple threads should produce correct results"() {
        given:
        def accountInMemoryRepository = new AccountInMemoryRepository()

        when:
        blitzer.blitz( () -> {
            accountInMemoryRepository.save(new Account())
        })

        then:
        blitzer.totalActionCount() == accountInMemoryRepository.count()
    }

    def "defensive copy should be created"() {
        given:
        def accountInMemoryRepository = new AccountInMemoryRepository()
        def account = new Account()
        def balance = BigDecimal.ONE
        account.setBalance(balance)

        when: "account is save in repository and changed after"
        accountInMemoryRepository.save(account)
        account.setBalance(BigDecimal.TEN)
        def optionalAccount = accountInMemoryRepository.findById(account.getId())

        then: "entity in repository is not changed"
        optionalAccount.isPresent() == true
        optionalAccount.get().getBalance() == balance
    }
}
