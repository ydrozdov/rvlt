package com.ydrozdov.revolut

import com.google.gson.Gson
import com.google.inject.Guice
import com.ydrozdov.revolut.controller.dto.AccountDTO
import com.ydrozdov.revolut.controller.dto.TransferDTO
import kong.unirest.JacksonObjectMapper
import kong.unirest.Unirest
import spock.lang.Shared
import spock.lang.Specification

class TransferFunctionalTest extends Specification {
    static final int APP_PORT = 1111
    static final String APP_HOST = "http://localhost"

    @Shared
    def injector
    def gson = new Gson()

    def setupSpec() {
        injector = Guice.createInjector(new AppModule())
        injector.getInstance(AppEntryPoint.class).start(APP_PORT)

        Unirest.config().setObjectMapper(new JacksonObjectMapper());
    }

    def "Accounts can be created and transfer can be made"() {
        given:
        def balance = 10

        when: "post request is sent to create an account"
        def account = Unirest.post("${APP_HOST}:${APP_PORT}/accounts")
            .header("Content-Type", "application/json")
            .body('{"balance":' + balance + '}')
            .asObject(AccountDTO.class)
            .getBody()

        then: "account is created"
        account != null
        account.getId() != null
        account.getBalance() == balance

        when: "post request is sent to create another account"
        def anotherAccount = Unirest.post("${APP_HOST}:${APP_PORT}/accounts")
            .header("Content-Type", "application/json")
            .body('{"balance":' + balance + '}')
            .asObject(AccountDTO.class)
            .getBody()

        then: "account is created"
        anotherAccount != null
        anotherAccount.getId() != null
        anotherAccount.getBalance() == balance

        when: "post request is sent to make a transfer"
        def transferDTO = TransferDTO.builder()
            .setAmount(5)
            .setSenderId(account.id)
            .setBeneficiaryId(anotherAccount.id)
            .build()

        def transfer = Unirest.post("${APP_HOST}:${APP_PORT}/transfers")
                .header("Content-Type", "application/json")
                .body(gson.toJson(transferDTO))
                .asObject(TransferDTO.class)
                .getBody()

        then: "transfer is made"
        transfer != null
        transfer.getId() != null

        when: "get request is sent to get a transfer"
        def returnedTransfer = Unirest.get("${APP_HOST}:${APP_PORT}/transfers/${transfer.getId()}")
                .header("Content-Type", "application/json")
                .asObject(TransferDTO.class)
                .getBody()

        then: "transfer is returned"
        returnedTransfer != null
        returnedTransfer.id == transfer.id
        returnedTransfer.senderId == transfer.senderId
        returnedTransfer.beneficiaryId == transfer.beneficiaryId
        returnedTransfer.amount == transfer.amount
    }

    def cleanupSpec() {
        injector.getInstance(AppEntryPoint.class).stop()
    }
}
