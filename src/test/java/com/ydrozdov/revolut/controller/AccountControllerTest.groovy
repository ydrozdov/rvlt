package com.ydrozdov.revolut.controller

import com.google.gson.Gson
import com.ydrozdov.revolut.controller.dto.AccountDTO
import com.ydrozdov.revolut.model.Account
import com.ydrozdov.revolut.service.AccountService
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.util.ContextUtil
import org.eclipse.jetty.http.HttpStatus
import org.modelmapper.ModelMapper
import spock.lang.Specification
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AccountControllerTest extends Specification {
    Gson gson = new Gson()
    def modelMapper = new ModelMapper()
    def res = Mock(HttpServletResponse)
    AccountController accountController

    @Unroll
    def "Account with negative balance should not be saved"(balance) {
        given:
        def accountDTO = AccountDTO.builder().setBalance(balance).build()
        def req = Stub(HttpServletRequest) {
            getInputStream() >> new ServletInputStreamImpl(
                new ByteArrayInputStream(gson.toJson(accountDTO).getBytes())
            )
        }
        Context ctx = ContextUtil.init(req, res)
        def accountService = Mock(AccountService)
        accountController = new AccountController(accountService, modelMapper)

        when: "create is called with negative balance"
        accountController.create(ctx)

        then: "BadRequestResponse is thrown and account is not saved"
        thrown(BadRequestResponse)
        0 * accountService.save(_)

        where:
        balance << [-1, -1.0, -500000.5]
    }

    def "Account with positive balance is saved"(balance) {
        given:
        def accountDTO = AccountDTO.builder().setBalance(balance).build()
        def req = Stub(HttpServletRequest) {
            getInputStream() >> new ServletInputStreamImpl(
                new ByteArrayInputStream(gson.toJson(accountDTO).getBytes())
            )
        }
        Context ctx = ContextUtil.init(req, res)
        def account = new Account()
        account.setId(UUID.randomUUID())
        account.setBalance(balance)
        def accountService = Mock(AccountService) {
            1 * save(_) >> account
        }
        accountController = new AccountController(accountService, modelMapper)

        when: "create is called with positive balance or zero"
        accountController.create(ctx)

        then:
        notThrown(BadRequestResponse)
        1 * res.setStatus(HttpStatus.CREATED_201)

        where:
        balance << [1, 0, 1.0, 8.8, 1000.09]
    }
}
