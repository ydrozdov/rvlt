package com.ydrozdov.revolut.controller

import com.google.gson.Gson
import com.ydrozdov.revolut.controller.dto.TransferDTO
import com.ydrozdov.revolut.model.Transfer
import com.ydrozdov.revolut.service.TransferService
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.util.ContextUtil
import org.eclipse.jetty.http.HttpStatus
import org.modelmapper.ModelMapper
import spock.lang.Specification
import spock.lang.Unroll

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TransferControllerTest extends Specification {
    def gson = new Gson()
    def modelMapper = new ModelMapper()
    def res = Mock(HttpServletResponse)
    TransferController transferController

    @Unroll
    def "Create shouldn't call execute with invalid input"(amount, senderId, beneficiaryId) {
        given:
        def transferDTO = TransferDTO.builder()
            .setSenderId(senderId)
            .setBeneficiaryId(beneficiaryId)
            .setAmount(amount)
            .build()

        def req = Stub(HttpServletRequest) {
            getInputStream() >> new ServletInputStreamImpl(
                    new ByteArrayInputStream(gson.toJson(transferDTO).getBytes())
            )
        }
        Context ctx = ContextUtil.init(req, res)
        def transferService = Mock(TransferService)
        transferController = new TransferController(transferService, modelMapper)

        when: "create with negative amount is called"
        transferController.create(ctx)

        then: "execute is not called"
        thrown(BadRequestResponse)
        0 * transferService.execute(_)

        where:
        amount  | senderId                             | beneficiaryId
        -1      | UUID.randomUUID().toString()         | UUID.randomUUID().toString()
        -5000.05| UUID.randomUUID().toString()         | UUID.randomUUID().toString()
        -10.08  | UUID.randomUUID().toString()         | UUID.randomUUID().toString()
        1.0     | "invalid uuid"                       | UUID.randomUUID().toString()
        2.0     | UUID.randomUUID().toString()         | "invalid uuid"
        -2.0    | "invalid uuid"                       | "invalid uuid"
    }

    @Unroll
    def "Create with valid input calls execute"(amount, senderId, beneficiaryId) {
        given:
        def transferDTO = TransferDTO.builder()
            .setSenderId(senderId)
            .setBeneficiaryId(beneficiaryId)
            .setAmount(amount)
            .build()
        def req = Stub(HttpServletRequest) {
            getInputStream() >> new ServletInputStreamImpl(
                    new ByteArrayInputStream(gson.toJson(transferDTO).getBytes())
            )
        }
        Context ctx = ContextUtil.init(req, res)
        def transfer = Transfer.builder()
            .setSenderId(senderId)
            .setBeneficiaryId(beneficiaryId)
            .setAmount(amount)
            .build()
        def transferService = Mock(TransferService) {
            1 * execute(_) >> transfer
        }
        transferController = new TransferController(transferService, modelMapper)

        when: "create is called with valid input"
        transferController.create(ctx)

        then: "execute is called"
        notThrown(BadRequestResponse)
        1 * res.setStatus(HttpStatus.CREATED_201)

        where:
        amount  | senderId                             | beneficiaryId
        1       | UUID.randomUUID().toString()         | UUID.randomUUID().toString()
        1.0     | UUID.randomUUID().toString()         | UUID.randomUUID().toString()
        10.0    | UUID.randomUUID().toString()         | UUID.randomUUID().toString()
    }

    def "Get with invalid transferId should fail validation"() {
        given:
        def req = Mock(HttpServletRequest)
        def transferService = Mock(TransferService)

        def pathParams = [transferId: "invalid uuid"]
        Context ctx = ContextUtil.init(req, res, "/:transferId", pathParams)
        transferController = new TransferController(transferService, modelMapper)

        when: "get is called with invalid transferId"
        transferController.get(ctx)

        then: "BadRequestResponse is thrown"
        thrown(BadRequestResponse)
        0 * transferService.get(_)
    }

    def "Get with valid transferId should call service"() {
        given:
        def req = Mock(HttpServletRequest)
        def pathParams = [transferId: UUID.randomUUID().toString()]
        Context ctx = ContextUtil.init(req, res, "/:transferId", pathParams)
        def transferService = Mock(TransferService) {
            1 * get(UUID.fromString(pathParams.transferId)) >> Transfer.builder()
                .senderId(UUID.randomUUID())
                .beneficiaryId(UUID.randomUUID())
                .amount(BigDecimal.ONE)
                .build()
        }
        transferController = new TransferController(transferService, modelMapper)

        when: "get is called with valid transferId"
        transferController.get(ctx)

        then: "service is called"
        notThrown(BadRequestResponse)
    }
}
