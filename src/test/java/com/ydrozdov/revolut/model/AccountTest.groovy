package com.ydrozdov.revolut.model

import org.jmock.lib.concurrent.Blitzer
import spock.lang.Specification
import spock.lang.Unroll

class AccountTest extends Specification {
    static final int ACTION_COUNT = 25000
    Blitzer blitzer = new Blitzer(ACTION_COUNT)

    def "Instance of Account can be created"() {
        when: "try to create an instance of Account"
        def account = new Account()
        then: "instance is created and id is generated and balance is zero"
        account != null
        account.id instanceof UUID
        account.id != null
        account.getBalance() == BigDecimal.ZERO
    }

    @Unroll
    def "Withdraw should deduct balance"(initBalance, amount, expected) {
        when: "withdraw is called"
        def account = new Account()
        account.setBalance(initBalance)
        account.withdraw(amount)

        then: "amount deducted from balance"
        expected == account.getBalance()

        where:
        initBalance | amount  || expected
        10.0        | 10.0    || BigDecimal.valueOf(0)
        10.0        | 5.0     || BigDecimal.valueOf(5)
        10.0        | 0       || BigDecimal.valueOf(10)
        0           | 0       || BigDecimal.valueOf(0)
        100         | -10     || BigDecimal.valueOf(90)
    }

    def "withdraw with null should throw an exception"() {
        when: "withdraw is called with null"
        def account = new Account()
        account.withdraw(null)

        then: "NullPointerException is thrown"
        thrown(NullPointerException)
    }

    @Unroll
    def "Deposit should increase balance"(initBalance, deposit, expected) {
        when: "deposit is called with a proper value"
        def account = new Account()
        account.setBalance(initBalance)
        account.deposit(deposit)

        then: "deposit added to the balance"
        expected == account.getBalance()

        where:
        initBalance | deposit || expected
        0.0         | 10.0    || BigDecimal.valueOf(10)
        10.0        | 10.0    || BigDecimal.valueOf(20)
        10.0        | 0       || BigDecimal.valueOf(10)
        0           | 0       || BigDecimal.valueOf(0)
        0           | -5      || BigDecimal.valueOf(5)
    }

    def "deposit with null should throw an exception"() {
        when: "deposit is called with null"
        def account = new Account()
        account.deposit(null)

        then: "NullPointerException is thrown"
        thrown(NullPointerException)
    }

    def "setBalance with null should throw an exception"() {
        when: "setBalance is called with null"
        def account = new Account()
        account.setBalance(null)

        then: "NullPointerException is thrown"
        thrown(NullPointerException)
    }

    def "Execute with multiple threads should produce correct results"() {
        given:
        def account = new Account()

        when:
        blitzer.blitz( () -> {
            account.deposit(BigDecimal.ONE)
        })

        then:
        blitzer.totalActionCount() == account.getBalance()
    }
}
