package com.ydrozdov.revolut.routing

import io.javalin.Javalin
import spock.lang.Specification

class AccountRoutingTest extends Specification {
    def "BindRoutes should call javalin.routes"() {
        given:
        def javalin = Mock(Javalin)
        def accountRouting = new AccountRouting(javalin)

        when: "bindRoutes is called"
        accountRouting.bindRoutes()

        then: "javalin.routes is called"
        1 * javalin.routes(_)
    }
}
