package com.ydrozdov.revolut.routing

import io.javalin.Javalin
import spock.lang.Specification

class TransferRoutingTest extends Specification {
    def "BindRoutes should call javalin.routes"() {
        given:
        def javalin = Mock(Javalin)
        def transferRouting = new TransferRouting(javalin)

        when: "bindRoutes is called"
        transferRouting.bindRoutes()

        then: "javalin.routes is called"
        1 * javalin.routes(_)
    }
}
