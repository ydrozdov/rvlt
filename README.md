# RESTful API  for money transfers between accounts.

## POST /accounts
+ Request
    + Headers

            Host:localhost:7000
            User-Agent:curl/7.54.0
            Accept:*/*
            Content-Type:application/json
            Content-Length:14

    + Body

            {"balance":10}

+ Response 201
    + Headers

            Date:Sat, 07 Mar 2020 10:50:57 GMT
            Server:Javalin
            Content-Type:application/json
            Content-Length:60

    + Body

            {"id":"a99a8224-c207-4c31-a271-1170c23a7708","balance":10.0}

## POST /transfers
+ Request
    + Headers

            Host:localhost:7000
            User-Agent:curl/7.54.0
            Accept:*/*
            Content-Type:application/json
            Content-Length:119

    + Body

            {"senderId":"a99a8224-c207-4c31-a271-1170c23a7708", "beneficiaryId":"f2a2d409-e124-46ca-977c-d028ea70e898","amount": 5}

+ Response 201
    + Headers

            Date:Sat, 07 Mar 2020 10:59:39 GMT
            Server:Javalin
            Content-Type:application/json
            Content-Length:163

    + Body

            {"senderId":"a99a8224-c207-4c31-a271-1170c23a7708","beneficiaryId":"f2a2d409-e124-46ca-977c-d028ea70e898","amount":5.0,"id":"c4a89477-6701-4cdc-9f97-05aa47e5167e"}
            
## GET /transfers/:transferId
+ Request
    + Headers

            Host:localhost:7000
            User-Agent:curl/7.54.0
            Accept:*/*
            Content-Type:application/json

    + Body



+ Response 200
    + Headers

            Date:Sat, 07 Mar 2020 11:02:11 GMT
            Server:Javalin
            Content-Type:application/json
            Content-Length:163

    + Body

            {"senderId":"a99a8224-c207-4c31-a271-1170c23a7708","beneficiaryId":"f2a2d409-e124-46ca-977c-d028ea70e898","amount":5.0,"id":"c4a89477-6701-4cdc-9f97-05aa47e5167e"}